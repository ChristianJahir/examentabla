let monto=20000;
let plazo=12;
let interes=(0.12);
let amortizacion=[];
let periodo=[];
let dias=[];
let fechainicio=[];
let fechafin=[];
let FechaI=moment('2021-08-03');
let FechaF=moment('2021-09-03');
let saldoInsoluto=[];
let intereses=[];
let iva=[];
let flujo=[];
const llenarTabla=document.querySelector('#tabla tbody');

calcularAmortizacion(monto,plazo);
calcularFechas();
calcularIntereses();
calcularIVA();
calcularFlujo();
function calcularAmortizacion(monto, plazo){
    amortizacion=monto/plazo;
    for(let i=1;i<plazo+1;i++){
        if (i<=1){
            saldoInsoluto[i]=monto;
        }else {
         saldoInsoluto[i]=saldoInsoluto[i-1]-amortizacion;
        }
    }
}

function calcularFechas(){
    for (i=1;i<plazo+1;i++){
    periodo[i]=[i];
    fechainicio[i]=FechaI.format('DD-MM-YYYY');
    fechafin[i]=FechaF.format('DD-MM-YYYY');
    dias[i]=(FechaF.diff(FechaI,'days')); //Calcula los dias entre los meses
    FechaI.add(1,'month'); //Aumenta el mes
    FechaF.add(1, 'month');
    }
}   
    
function calcularIntereses(){
    for (i=1;i<plazo+1;i++){
        intereses[i]=saldoInsoluto[i]*dias[i]*(interes/360);
    }
}

function calcularIVA(){
    for (i=1;i<plazo+1;i++){
    iva[i]=intereses[i]*.16;
    }   
}
function calcularFlujo(){
    for (i=1;i<plazo+1;i++){
    flujo[i]=amortizacion+intereses[i]+iva[i];
    }
}
for (i=1;i<plazo+1;i++){
const row1=document.createElement('tr')
    row1.innerHTML=`
    <td>${periodo[i]}</td>
    <td>${fechainicio[i]}</td>
    <td>${fechafin[i]}</td>
    <td>${dias[i]}</td>
    <td></td>
    <td>${saldoInsoluto[i]}</td>
    <td>${amortizacion}</td>
    <td>${intereses[i]}</td>
    <td>${iva[i]}</td>
    <td>${flujo[i]}</td>
    `;
    llenarTabla.appendChild(row1);
}